
Module: Exit Bee
Author: Exit Bee <info@exitbee.com>


INTRODUCTION
===========
This module adds the Exit Bee tracking code to your website.

REQUIREMENTS
============
* Exit Bee user account

INSTALLATION
============
Copy the 'exitbee' module directory in your Drupal
sites/all/modules directory as usual.

CONFIGURATION
============
No need for any further configuration.

USAGE
=====
In the settings page enter your Exit Bee Website Key.

All website pages will now have the required JavaScript added to the
HTML footer, can confirm this by viewing the page source from
your browser.

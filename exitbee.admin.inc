<?php

/**
 * @file
 * Administrative page callbacks for the exitbee module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function exitbee_admin_settings_form($form_state) {

  $form['exitbee_website_key'] = array(
    '#title' => t('Website Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('exitbee_website_key', ''),
    '#size' => 100,
    '#required' => TRUE,
    '#description' => t('The Website Key is unique to each site you add to your Exit Bee account. <a href="@website_key" target="_blank">Where is the Website key?</a>', array('@website_key' => 'http://support.exitbee.com/cms-integrations/where-is-the-website-key')),
  );

  return system_settings_form($form);
}

/**
 * Implements _form_validate().
 */
function exitbee_admin_settings_form_validate($form, &$form_state) {

  // Trim Website Key value.
  $form_state['values']['exitbee_website_key'] = trim($form_state['values']['exitbee_website_key']);

  $uuid = '/^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i';
  if (!preg_match($uuid, $form_state['values']['exitbee_website_key'])) {
    form_set_error('exitbee_website_key', t('A valid Exit Bee Website Key is case sensitive and formatted like xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx.'));
  }
}

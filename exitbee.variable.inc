<?php

/**
 * @file
 * Definition of variables for Variable API module.
 */

/**
 * Implements hook_variable_info().
 */
function exitbee_variable_info($options) {
  $variables['exitbee_website_key'] = array(
    'type' => 'string',
    'title' => t('Website Key', array(), $options),
    'default' => '',
    'description' => t('The Website Key is unique to each site you add to your Exit Bee account. <a href="@website_key">Where is the Website key?</a>.', array('@website_key' => 'http://support.exitbee.com/cms-integrations/where-is-the-website-key')),
    'required' => TRUE,
    'group' => 'exitbee',
    'localize' => TRUE,
    'multidomain' => TRUE,
    'validate callback' => 'exitbee_validate_exitbee_website_key',
  );

  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function exitbee_variable_group_info() {
  $groups['exitbee'] = array(
    'title' => t('Exit Bee'),
    'description' => t('Allows your site to run Exit Bee campaigns by adding a Javascript tracking code to every page.'),
    'access' => 'administer exitbee',
    'path' => array('admin/config/system/exitbee'),
  );

  return $groups;
}

/**
 * Validate Website Key variable.
 */
function exitbee_validate_exitbee_website_key($variable) {
  // Trim value.
  $variable['value'] = trim($variable['value']);
  $uuid = '/^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i';

  if (!preg_match($uuid, $variable['value'])) {
    return t('A valid Exit Bee Website Key is case sensitive and formatted like xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx.');
  }
}

(function(doc) {
  var xtb = document.createElement("script");
  xtb.type = "text/javascript";
  xtb.async = true;
  xtb.src = "https://" + Drupal.settings.exitbee.eb_base_url + "/c/" + Drupal.settings.exitbee.eb_website_key + "/exitbee.js";
  document.getElementsByTagName("head")[0].appendChild(xtb);
}());